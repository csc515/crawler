package missouristate.edu;

/**
 * CRAWL ME ASSIGNMENT (HW3)
 * @author PLACE YOUR FIRST AND LAST NAME HERE
 * Key = ADD YOUR KEY HERE
 * 
 * Your objective is to use JSoup to crawl the website:
 *    http://www.techdiggity.com/crawlme
 *    
 * Make sure to set your user-agent to: MSU Web Browser 1.0
 * 
 * Make sure to grab cookies from the current request and send
 * them with the next request.    
 *    
 * First find out what your password is by visiting (with JSoup):
 *    http://www.techdiggity.com/crawlme/login/getPassword?emailAddress=YOUR_EMAIL_ADDRESS
 * 
 * Next, once you have your password, visit:
 *   http://www.techdiggity.com/crawlme/login
 *   
 * Make note of the inputs on this page. Create your data map and
 * include all of the inputs/values from the web form.
 * 
 * Finally, make a POST request to:
 *   http://www.techdiggity.com/crawlme/login
 * with the correct user-agent, cookies, and data
 *  
 * If you get the document from the JSoup Response and then 
 * get the body from the document, and then get the HTML from 
 * the body, you will find your KEY. Add this key to line 6 of this class.
 * 
 * Copy all of the text in this class and submit it to TEAMS. Don't forget
 * that there is a two step process for submission.  
 * 
 */
public class Crawler {
	private static final String ROOT_URL = "http://www.techdiggity.com/crawlme";
	
	public Crawler() throws Exception {
		// Visit the getPassword URL (ROOT_URL + /login/getPassword?emailAddress=YOUR_EMAIL_ADDRESS).
		// If you see an error in your response which shows the URL you need to visit: 
		//              /login/getPassword?emailAddress\u003dYOUR_EMAIL_ADDRESS
		// Realize that the \u003d is simply the equals sign
		// You may want to print the response to check what is sent back to you.

		
		// Get the password from the Document/Response
		
		
		// Visit the login site, Get/Store Cookies
		// You may want to print the response to check what is sent back to you.
		
		
		
		// Get the token from the page (Document/Response)
		
				
		
		// Get Data (Build data variable; Map<String, String>)
		// This data map will need to be sent when you POST the 
		// next request.
		
		
		// Post the data/cookies to the post login url
		// You will see an error or you will see the key sent back to you.
		// You may want to print the response to check what is sent back to you.
		
		
		
	}


	public static void main(String[] args) throws Exception {
		new Crawler();
	}

}
